FROM node:12.9.0-alpine

WORKDIR /usr/src/todo-fe

COPY package.json .

RUN npm install

COPY . ./

EXPOSE 3000

CMD ["npm", "start"]